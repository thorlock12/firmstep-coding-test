from datetime import datetime
from collections import defaultdict
import requests
import sys

def fetch_url(url):
    return requests.get(url).text

def get_lines(path):
    with open(path, 'r') as f:
        return f.readlines()

def get_events(lines):
    events_started = False
    events = []
    next_event = {}
    for l in lines:
        if not l:
            continue

        line = l.strip()
        if not events_started and line.startswith("BEGIN:VEVENT"):
            events_started = True
        elif not events_started:
            continue

        if line.startswith("END:VEVENT"):
            events.append(next_event)
            continue
        elif line.startswith("BEGIN:VEVENT"):
            next_event = {}
            continue

        if line.startswith("DTSTART"):
            key = "DTSTART"
            _, value = line.replace("\n", "").split(":", 2)
            date = datetime.strptime(value, "%Y%m%d")
            next_event[key] = date
        else:
            key, value = line.split(":", 2)
            next_event[key] = value.replace("\n", "")

    return sorted(events, key=lambda e: e["DTSTART"])


def group_events_by_day(events):
    event_days = defaultdict(list)
    for event in events:
        event_days[event["DTSTART"]].append(event)
    return event_days


def find_next_collections(date, grouped_events):
    for group_date, events in grouped_events.items():
        if group_date < date:
            continue
        return events


def print_details(events):
    print("Date: {}".format(events[0]["DTSTART"].strftime("%Y-%m-%d")))
    for event in events:
        print("SUMMARY: {}\nUID: {}".format(event["SUMMARY"], event["UID"]))


if __name__ == "__main__":
    lines = fetch_url("https://s3-eu-west-1.amazonaws.com/fs-downloads/GM/binfeed.ical")
    events = get_events(lines.split("\n"))
    grouped_events = group_events_by_day(events)
    date = datetime.strptime(sys.argv[1], "%Y-%m-%d")
    next_events = find_next_collections(date, grouped_events)
    if next_events is None:
        print("No events found")
    else:
        print_details(next_events)
