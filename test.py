from datetime import datetime
from bin import (
    get_events,
    group_events_by_day,
    find_next_collections
)

def test_find_event():
    date = datetime(2018, 5, 8)
    lines = """
        BEGIN:VEVENT
        UID:20180103T190905GMT-4408pxTU9I@192.124.249.105
        DTSTAMP:20180103T190905Z
        DTSTART;VALUE=DATE:20180508
        SUMMARY:Black Bin Collection
        END:VEVENT"""
    events = get_events(lines.split("\n"))
    grouped_events = group_events_by_day(events)
    next_events = find_next_collections(datetime(2018, 5, 7), grouped_events)

    assert len(events) == 1
    assert date in grouped_events
    assert len(next_events) == 1
    assert next_events[0]["SUMMARY"] == "Black Bin Collection"


def test_find_correct_event():
    date1 = datetime(2018, 5, 6)
    date2 = datetime(2018, 5, 8)

    lines = """
        BEGIN:VEVENT
        UID:20180103T190905GMT-4408pxTU9I@192.124.249.105
        DTSTAMP:20180103T190905Z
        DTSTART;VALUE=DATE:20180506
        SUMMARY:Black Bin Collection
        END:VEVENT
        BEGIN:VEVENT
        UID:20180103T190905GMT-4408pxTU9I@192.124.249.105
        DTSTAMP:20180103T190905Z
        DTSTART;VALUE=DATE:20180508
        SUMMARY:Blue Bin Collection
        END:VEVENT"""
    events = get_events(lines.split("\n"))
    grouped_events = group_events_by_day(events)
    next_events = find_next_collections(datetime(2018, 5, 7), grouped_events)

    assert len(events) == 2
    assert date1 in grouped_events
    assert date2 in grouped_events
    assert len(next_events) == 1
    assert next_events[0]["SUMMARY"] == "Blue Bin Collection"


def test_find_two_events():
    lines = """
        BEGIN:VEVENT
        UID:20180103T190905GMT-4408pxTU9I@192.124.249.105
        DTSTAMP:20180103T190905Z
        DTSTART;VALUE=DATE:20180508
        SUMMARY:Black Bin Collection
        END:VEVENT
        BEGIN:VEVENT
        UID:20180103T190905GMT-4408pxTU9I@192.124.249.105
        DTSTAMP:20180103T190905Z
        DTSTART;VALUE=DATE:20180508
        SUMMARY:Blue Bin Collection
        END:VEVENT"""
    events = get_events(lines.split("\n"))
    grouped_events = group_events_by_day(events)
    next_events = find_next_collections(datetime(2018, 5, 7), grouped_events)

    summaries = [n["SUMMARY"] for n in next_events]

    assert len(events) == 2
    assert len(next_events) == 2
    assert "Black Bin Collection" in summaries
    assert "Blue Bin Collection" in summaries



if __name__ == "__main__":
    test_find_event()
    test_find_two_events()
    test_find_correct_event()
